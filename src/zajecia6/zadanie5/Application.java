package zajecia6.zadanie5;

import java.util.Scanner;

public class Application {

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Witaj w programie: ");
        System.out.println("Podaj nazwe firmy: ");
        String name = scanner.nextLine();
        Company myCompany = new Company(name);

        printMenu();

        int wybor = scanner.nextInt();
        switch (wybor){
            case 1:
                addEmployee(myCompany);
                break;
            case 2:

                print(myCompany);

                break;
            default:
                break;
        }

    }

    private static void print(Company myCompany) {
        for(Pracownik e: myCompany.getEmployees()){
            if (e!=null){
                System.out.println(e.getDecription());
            }
        }
    }

    private static void addEmployee(Company myCompany) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj imie: ");
        String empname = scanner.nextLine();
        System.out.println("Podaj nazwisko");
        String empsurname = scanner.nextLine();
        System.out.println("Podaj pensje: ");
        double salary = scanner.nextDouble();
        System.out.println("Podaj wiek: ");
        int age = scanner.nextInt();
        System.out.println("Podaj email: ");
        String email = scanner.nextLine();

        Pracownik employee = new Pracownik(empname, empsurname, salary, age, email);
        boolean isSuccess = myCompany.addEmployee(employee);
        if(isSuccess){
            System.out.println("Dodano pracownika");
        }else{
            System.out.println("Nie udało się dodać - zbyt dużo pracowników");
        }
    }

    private static void printMenu() {
        System.out.println("Wybierz obecje");
        System.out.println("1. Dodawanie pracownika: ");
        System.out.println("2. Wyświetlanie pracowników: ");
        System.out.println("Wybór: ");
    }
}
