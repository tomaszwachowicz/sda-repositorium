package zajecia6.zadanie5;

public class Company {

    private static final int DEFAULT_SIZE = 50;
    private String name;
    private Pracownik[] employees;
    private int complanySize = 0;


    public Company(String name) {
        this.name = name;
        this.employees = new Pracownik[DEFAULT_SIZE]; // zagospodarowanie miejsca na wprowadzanie pracowników
    }

    public Company(String name, int initialSize) {
        this.name = name;
        this.employees = new Pracownik[initialSize]; // wprowadzenie górnej granicy z ręki
    }

    public boolean addEmployee(Pracownik emp) {
        if (complanySize < employees.length) {
            employees[complanySize++] = emp;
            return true;
        }
        return false;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Pracownik[] getEmployees() {
        return employees;
    }
}
