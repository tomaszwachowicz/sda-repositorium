package zajecia6.zadanie5;

public class Pracownik {

    private String name;
    private String surname;
    private String email;
    private int age;
    private double salary;


    public Pracownik(String name, String surname){
        this.name = name;
        this.surname = surname;

    }

    public Pracownik(String name, String surname, double salary){
        this(name, surname);
        this.salary = salary;

    }

    public Pracownik(String name, String surname, double salary, int age){
        this(name, surname, salary);
        this.age = age;

    }

    public Pracownik(String name, String surname, double salary, int age, String email){
        this(name, surname,salary, age);
        this.email = email;

    }

    public String getName(){return name;}

    public void setName(String name){


        this.name = name;

    }

    public String getSurname(){return surname;}

    public void setSurname(String surname){


        this.surname = surname;

    }

    public String getEmail(){return email;}

    public void setEmail(String email){
        this.email = email;
    }

    public int getAge(){return age;}

    public void setAge(int age){
        this.age = age;
    }

    public double getSalary(){return salary;}

    public void setSalary(double salary){
        this.salary = salary;
    }

    public String getDecription(){
        return String.format("name: %s, Surname: %s, salary: %f",name,surname,salary);
    }

}
