package zajecia6;

public class Rectangle {


    //pola do opisywania właściwosci prostokata
    private double a;
    private double b;

    //geterr i setter dla pola a

    public double getA(){
        return a;
    }
    public void setA(int a){
        if(a <=0){
            throw new IllegalArgumentException("Długość mniejsza od 0");
        }
        this.a = a; // referencja do tego obiektu ktory wywołał jakąś metode
    }

    //getter i setter dla pola b
    public double getB(){
        return b;
    }
    public void setB(double b){

        if(b <=0){
            throw new IllegalArgumentException("Długość mniejsza od 0");
        }

        this.b = b;

    }



    // metoda oblicz pole

    public double field(){

        return a*b;

    }

    // metoda obliczajaca obwod

    public double circumference(){

        return 2*(a+b);
    }

}