package zajecia6.zadanie2;

public class KartezianPoint {

    private double x;
    private double y;

    //konstruktor bez parametrowy

/*    public KartezianPoint(){

    }*/

    // konstruktor przeciazony

    public KartezianPoint(double x,double y){
        this.x =x;
        this.y =y;
    }

    public double getX(){return x;}

    public void setX(double x){this.x =x;}

    public double getY(){return y;}

    public void setY(double y){this.y =y;}


    //metoda obliczająca odleglość od środka

    public double wector(){

        return Math.sqrt((x*x+y*y));

    }
}
