package zajecia7.math;

import java.util.Random;

public class Matrix {

    private int[][] matrix;
    private int x;
    private int y;

    /**
     * creates new matrix
     *
     * @param x
     * @param y
     */

    public Matrix(int x, int y) {

        this.x = x;
        this.y = y;
        matrix = new int[x][y];

    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }


    /**
     * Adds second matrix given as a parameter
     * @param secondMatrix
     * @return
     */

    public Matrix addMatrix(Matrix secondMatrix) {

        //najpierw sprawdzic czy wymiray drugiej sie zgadzaja

        //przejdz do obliczen
        Matrix result = new Matrix(this.x, this.y);

        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {

                result.matrix[i][j] = this.matrix[i][j] + secondMatrix.matrix[i][j];

            }


        }
        return result;
    }

    public void print() {

        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {

                System.out.print(matrix[i][j] + " ");


            }
        System.out.println();

        }

    }

    /**
     * fill Matrix with random values
     */

    public void fillWithRandomValues(){

        Random random = new Random();
            for (int i=0; i<x;i++){
                for(int j=0;j<y;j++){
                    matrix[i][j]= random.nextInt(50);
                }
            }

    }
}
