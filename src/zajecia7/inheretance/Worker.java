package zajecia7.inheretance;

public class Worker extends Person {


    private double salary;
    private String position;

    public Worker(String name, String surname, int age) {
        super(name, surname, age);
    }

    public Worker(String name, String surname, int age, double salary, String position) {
        super(name, surname, age);
        this.salary = salary;
        this.position = position;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return String.format("%s %s mam lat %d i Pracuje jako %s zarabiam %f", getName(), getSurname(), getAge(), getPosition(), getSalary());
    }

}
