package zajecia7.inheretance;

public class Application {

    public static void main(String[] args) {

        Person person = new Person("Piotr","kowalski",20);
        System.out.println(person.describe());

        Student student = new Student("Piotr","Kowalski",20);
        System.out.println(student.describe());

        Student studentInformatyki = new Student("Kasia","Pawlak",22,12222,"Politechnika");

        System.out.println(studentInformatyki.describe());

        Worker pracownik = new Worker("Tadeusz","Wrona",90,15000, "operator maszyny");

        System.out.println(person);

        System.out.println(studentInformatyki);

        System.out.println(pracownik);

    }



}
