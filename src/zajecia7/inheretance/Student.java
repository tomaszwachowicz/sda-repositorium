package zajecia7.inheretance;

// l;asa rozszerzajaca,pochodna - sub class
public class Student extends Person {

    private int index;
    private String kierunek;
    private String university;


    public Student(String name, String surname, int age) {
        super(name, surname, age);
    }
    // wywołanie konstruktora z nad klasy

    public Student(String name, String surname, int age, int index, String university) {
        super(name, surname, age);
        this.index = index;
        this.university = university;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getKierunek() {
        return kierunek;
    }

    public void setKierunek(String kierunek) {
        this.kierunek = kierunek;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }


    @Override
    public String describe() {

        System.out.println("Numer indeksu " + getIndex());
        String s = super.describe();
        return String.format("%s i Jestem studentem %s", s, getUniversity());
/*        return String.format("%s %s mam lat %d i Jestem studentem %s", getName(), getSurname(), getAge(), getUniversity());*/
    }

    @Override
    public String toString() {
        return String.format("%s %s mam lat %d i Jestem studentem %s", getName(), getSurname(), getAge(), getUniversity());
    }
}
