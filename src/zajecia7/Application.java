package zajecia7;

import zajecia7.math.Matrix;

public class Application {

    public static void main(String[] args) {


        Matrix first = new Matrix(3,3);
        first.fillWithRandomValues();
        first.print();
        System.out.println();

        Matrix second = new Matrix(3,3);
        second.fillWithRandomValues();
        second.print();
        System.out.println();

        System.out.println("Result:");
        Matrix result = first.addMatrix(second);
        result.print();

    }

}
