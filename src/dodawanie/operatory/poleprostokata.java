package dodawanie.operatory;

import java.util.Scanner;

public class poleprostokata {

    public static void main(String[] args){


        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj boki prostokąta");
        double a = sc.nextDouble();
        double b = sc.nextDouble();


        //policzyć pole
        double pole = a * b;
        System.out.println("Pole prostokąta o krawędziach a = "+a+" i "+b+" wynosi " + pole);


        //policzyć obwód
        double obwod = 2*a + 2*b;
        System.out.println("Obwód prostokąta o krawędziach a = "+a+" i "+b+"  wynosi " + obwod);


        double duzepole = 100;
        System.out.println(duzepole);

        System.out.println("Czy duże pole "+ (pole > duzepole));


        if(pole > duzepole){
            System.out.println("Pierwsze pole jest wieksze");
        } else {
            System.out.println("Drugie pole jest wieksze");
        }

    }
}
