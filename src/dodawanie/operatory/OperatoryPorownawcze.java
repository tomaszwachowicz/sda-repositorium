package dodawanie.operatory;

public class OperatoryPorownawcze {

    public static void main(String[] args){

        int a = 15;
        int b = 20;

        System.out.println("A =" + a +" B = "+ b);


        // do zmiennej boolean o nazwie czyrówne wpisz operator =
        //wynik operacji porównania
        boolean czyRowne = a == b;
        System.out.println("Czy równe "+ czyRowne);


        // do zmiennej boolean o nazwie czyrówne wpisz operator !=
        //wynik operacji porównania
        boolean czyRozne = a != b;
        System.out.println("Czy różne "+ czyRozne);


        // do zmiennej boolean o nazwie czy wieksze wpisz operator =
        //wynik operacji porównania
        boolean czyWieksze = a > b;
        System.out.println("Czy większe "+ czyWieksze);

        // do zmiennej boolean o nazwie czy mniejsze wpisz operator =
        //wynik operacji porównania
        boolean czyMniejsze = a < b;
        System.out.println("Czy mniejsze "+ czyMniejsze);

    }
}
