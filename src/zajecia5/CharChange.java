package zajecia5;

import java.util.Scanner;

public class CharChange {

    public static void main(String[] args) {


        System.out.println("Podaj zdanie skłądające sie z 20 znaków");
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();

        if (text.length() < 20) {

            printMenu();
            int choice = scanner.nextInt();
            String wynik = " ";
            switch (choice) {
                case 1:
                    wynik = toUpper(text);
                    System.out.println(wynik);
                    break;
                case 2:
                    wynik=toLover(text);
                    System.out.println(wynik);
                    break;
                case 3:
                    wynik=toggleCase(text);
                    System.out.println(wynik);
                    break;

            }


        } else {
            System.out.println("Napis zbyt długi");
        }


    }

    public static void printMenu() {

        System.out.println("1. Wszystkie litery na duze");
        System.out.println("2. Wszystkie litery na małe");
        System.out.println("3. Duże na małe i małe na duze");

    }

    public static String toUpper(String input){
        String result = " ";
        for (int i = 0; i < input.length(); i++){

            char element = input.charAt(i);
            if(element >= 97 && element <= 122){

                element -=32;

            }

            result += element;
        }
        return result;

    }

    public static String toLover(String input){

        String result = " ";
        for (int i = 0; i < input.length(); i++){

            char element = input.charAt(i);
            if(element >= 65 && element <= 90){

                element +=32;

            }

            result += element;

        }
        return result;



    }

    public static String toggleCase(String str){

StringBuilder sb = new StringBuilder();
for (int i = 0; i < str.length();i++){

    char element = str.charAt(i);
    if (element >= 65 && element <= 90){

        element+=32;

    } else if(element >= 97 && element <= 122){

        element -= 32;

    }

    sb.append(element);

}

String result = sb.toString();

        return result;




    }
}
