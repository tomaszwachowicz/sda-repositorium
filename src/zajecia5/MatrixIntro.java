package zajecia5;

import java.util.Scanner;


public class MatrixIntro {


    public static void fillMatrix(int[][] m) {

        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {

                m[i][j] = scanner.nextInt();

            }

        }


    }

    public static void printMatrix(int[][] matrix) {

        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {

                System.out.print(matrix[i][j] + " | ");

            }

            System.out.println();

        }

        System.out.println();


    }


    public static int[][] addMatrix(int[][] a, int[][] b) {


        //  obsłużyć nie pasujace

        if (validate(a, b) == false) {

            return null;

        }


        int[][] result = new int[a.length][a[0].length];

        for (int i = 0; i < result.length; i++) {

            for (int j = 0; j < result[i].length; j++) {

                result[i][j] = a[i][j] + b[i][j];

            }

        }
        return result;

    }

    private static boolean validate(int[][] a, int[][] b) {

        boolean areSame = true;
        if (a.length != b.length) {
            return false;
        }

        for (int i = 0; i < a.length; i++) {
            //jezeli jest rozna liczba kolumn to zwroc falsz
            if (a[i].length != b[i].length) {
                return false;
            }

        }

        // jezeli dzoslismy az tutaj to znaczy ze wszystkie sa takie samem zwroc prawde
        return true;
    }

    public static void main(String[] args) {

        // dopisac pobieranie liczba wierszy i kolumn dla obu macierzy

        //1 macierz

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj rozmiar pierwszej macierzy ");
        System.out.println("Podaj ilość wierszy ");
        int mx = scanner.nextInt();
        System.out.println("Podaj ilość kolumn ");
        int my = scanner.nextInt();
        System.out.println("Macierz ma wielkosc: " + mx + "x" + my);

        //2 macierz

        System.out.println("Podaj rozmiar drugiej macierzy ");
        System.out.println("Podaj ilość wierszy ");
        int smx = scanner.nextInt();
        System.out.println("Podaj ilość kolumn ");
        int smy = scanner.nextInt();
        System.out.println("Macierz ma wielkosc: " + smx + "x" + smy);

        // utworzenie macierzy 3x3
        int[][] matrix = new int[mx][my];
        int[][] secondMatrix = new int[smx][smy];

        //wypełnienie wartościami od użytkownika

        fillMatrix(matrix);
        fillMatrix(secondMatrix);


        //drukowanie

        printMatrix(matrix);
        printMatrix(secondMatrix);


        //
        int[][] result = addMatrix(matrix, secondMatrix);

        //drukowanie wyniku z dodawania macierzy
        if (result != null) {
            printMatrix(result);
        } else {

            System.out.println(" Nie można dodać takich macierzy");

        }


    }

}
