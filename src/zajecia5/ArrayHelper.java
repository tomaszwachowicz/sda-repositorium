package zajecia5;


import org.jetbrains.annotations.Contract;

//Klasa pomocnicza - zawoiera metody do poeracji na tabelach
public class ArrayHelper {

    /**
     *      metoda sprawdzająca w tablicy integerow, czy podany element wystepuje
     */



    public static boolean isPresent(int[] array, int element){

        boolean present = false;

        for(int i = 0; i <array.length; i++){
            if(array[i] == element){
                present = true;
                break;
            }

        }
        return present;
    }


    /**
     *  Metoda wyświetlajaca wszystkie elementy tablicy (dla tablicy int)
     *  w jednej linii
     */

    public static void elementsArray(int[] array){

        for(int i=0;i<array.length;i++){
            System.out.print(array[i] + " ");
            if(i % 20 ==0){
                System.out.println();
            }
        }
        System.out.println();
    }


}
