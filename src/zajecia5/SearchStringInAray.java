package zajecia5;

import java.util.Scanner;

public class SearchStringInAray {

    public static void main(String[] args) {

        // utworzyc tablice 5 napisow
        // i od razu je zainicjowac
        // I wersja
        String[] nazwiska = new String[5];
        nazwiska[0] = "Kowalski";
        nazwiska[1] = "Adamiak";
        nazwiska[2] = "Nowacki";
        nazwiska[3] = "Nowak";
        nazwiska[4] = "Kowal";

        // II wersja

        String[] surnames = {"Kowalski", "Adamiak", "Nowacki", "Nowak", "Kowal"};

        // Zapytac uzytkownika o nazwisko


        System.out.println("Podaj nazwisko");

        Scanner scanner = new Scanner(System.in);

        String nazwisko = scanner.nextLine();
        boolean isPresent = false;

        for(int i =0;i < surnames.length;i++){
            if(surnames[i].equals(nazwisko) ){

                isPresent = true;
                break;

            }


        }
        System.out.println(isPresent);


        // odpowiedzuec czy takie nazwisko wystepuje

    }
}
