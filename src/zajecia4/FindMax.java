package zajecia4;

import java.util.Random;

public class FindMax {

    public static void main(String[] args) {


        Random random = new Random();

        int[] array = new int[10];

        //wpisywanie losowych wartości

        for (int i = 0; i < array.length; i++) {

            array[i] = random.nextInt(50);

        }

        // wypisz wartości

        for (int i = 0; i < array.length; i++) {

            System.out.print(array[i] + " ");

        }
        //zakłądamy wstępnie, ze najwieksza wartosc jest w tablicy o indeksie 0
        int max = array[0];

        for (int i = 1; i < array.length; i++) {
            if (array[i] > max) {

                max = array[i];

            }

        }

        System.out.println("\nNajwiększa wartosć to: " + max);
    }
}
