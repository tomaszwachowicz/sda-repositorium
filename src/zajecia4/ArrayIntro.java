package zajecia4;

import java.util.Scanner;

public class ArrayIntro {

    public static void main(String[] args) {

        // utworzenie 10 elementowej tablicy
        int[] tab = new int[10];

/*        tab[0] = 25;
        tab[1] = 20;
        tab[2] = 50;*/

// wypełnić tabele elementami takimi, że w każdym jest indeks + 20

        //wypełnia
        for (int i = 0; i < tab.length; i++) {

            tab[i] = i + 20;

        }

        //drukuje

        for (int i = 0; i < tab.length; i++) {

            System.out.println("Index: " + i + " Wartość: " + tab[i]);
        }

        int suma = 0;

        for (int i = 0; i < tab.length; i++) {
            suma += tab[i];
        }


        double srednia = (double) suma / (tab.length);


        System.out.println("Suma elementów tablicy " + suma + " średnia wynosi " + srednia);


//        Scanner scanner = new Scanner(System.in);


    }

}
