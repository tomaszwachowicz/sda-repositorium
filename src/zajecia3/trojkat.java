package zajecia3;

import java.util.Scanner;

public class trojkat {

    public static double poleTrojkata(double a, double b, double c) {


        double p = (a + b + c) / 2;


        return Math.sqrt(p * (p - a) * (p - b) * (p - c));

    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);


        //boki trójkąta
        System.out.println("Podaj bok A");
        double a = sc.nextDouble();

        System.out.println("Podaj bok B");
        double b = sc.nextDouble();

        System.out.println("Podaj bok C");
        double c = sc.nextDouble();


        if (a + b > c && a + c > b && c + b > a) {

           double wynik = poleTrojkata(a,b,c);

            System.out.println("Trójkąt o polu " + String.format("%.2f",wynik) + " i obwodzie " + String.format("%.2f", (a + b + c)));
        } else {
            System.out.println("To nie może być trójkąt");
        }


    }
}
