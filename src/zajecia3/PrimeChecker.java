package zajecia3;

import java.util.Scanner;

public class PrimeChecker {

    public static boolean isPrime(int number) {
        boolean result = true;


        for (int i = 2; i < number; i++) {

            if (number % i == 0) {

                result = false;
                break;
            } else if (i != 1){
                result = false;
                break;
            }

        }
        return result;
    }


    public static void main(String[] args) {

        System.out.println("Podaj liczbę");
        Scanner sc = new Scanner(System.in);
        int liczba = sc.nextInt();
        boolean wynik = isPrime(liczba);

        if (wynik == false) {

            System.out.println("Czy liczba " + liczba + " jest pierwsza? nie jest");
        } else {

            System.out.println("Czy liczba " + liczba + " jest pierwsza? jest");
        }
/*        System.out.println("Czy liczba jest pierwsza?: " + liczba + ": " + wynik);*/


    }
}
